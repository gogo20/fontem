﻿using System;
using System.IO;
using Fontem.TransactionHistory.Core.Common;
using Fontem.TransactionHistory.Core.Consumers;
using Fontem.TransactionHistory.Core.Services;
using MassTransit;
using MetroBus;
using MetroBus.Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;

namespace Fontem.TransactionHistory.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            new HostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(basePath: Directory.GetCurrentDirectory());
                    config.AddJsonFile("appsettings.json", optional : true);
                    config.AddEnvironmentVariables();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddLogging();

                    services.AddHttpClient(Constants.ResilientHttpClient)
                    .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
                    {
                                    TimeSpan.FromSeconds(1),
                                    TimeSpan.FromSeconds(5),
                                    TimeSpan.FromSeconds(10)
                    }));

                    string rabbitMqUri = hostContext.Configuration.GetValue<string>("RabbitMqUri");
                    string rabbitMqUserName = hostContext.Configuration.GetValue<string>("RabbitMqUserName");
                    string rabbitMqPassword = hostContext.Configuration.GetValue<string>("RabbitMqPassword");

                    services.AddScoped<ITransactionHistoryService, TransactionHistoryService>();

                    services.AddMetroBus(x =>
                    {
                        x.AddConsumer<OrderCompletedEventConsumer>();
                    });

                    services.AddSingleton<IBusControl>(provider => MetroBusInitializer.Instance
                            .UseRabbitMq(rabbitMqUri, rabbitMqUserName, rabbitMqPassword)
                            .RegisterConsumer<OrderCompletedEventConsumer>("fontem.transaction.history.queue", provider)
                            .Build())
                        .BuildServiceProvider();

                    services.AddHostedService<BusService>();
                })
                .RunConsoleAsync().Wait();
        }
    }
}