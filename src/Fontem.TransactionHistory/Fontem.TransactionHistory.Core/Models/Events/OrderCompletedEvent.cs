using System;

namespace Fontem.Checkout.Core.Models.Events
{
    public class OrderCompletedEvent
    {
        public Guid OrderId { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CorrelationId { get; set; }
    }
}