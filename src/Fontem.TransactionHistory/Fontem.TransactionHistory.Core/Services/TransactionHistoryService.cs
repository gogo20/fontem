using System;
using System.Net.Http;
using System.Threading.Tasks;
using Fontem.TransactionHistory.Core.Dtos.Requests;
using Microsoft.Extensions.Logging;

namespace Fontem.TransactionHistory.Core.Services
{
    public class TransactionHistoryService : ITransactionHistoryService
    {
        private readonly ILogger<TransactionHistoryService> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        
        public TransactionHistoryService(ILogger<TransactionHistoryService> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public async Task AddAsync(AddTransactionHistoryRequestDto request)
        {
            // Let's assume we are calling the Transaction History API in order to create transaction logs asynchronously.

            await Console.Out.WriteLineAsync(request.OrderId.ToString());
        }
    }
}