using System.Threading.Tasks;
using Fontem.TransactionHistory.Core.Dtos.Requests;

namespace Fontem.TransactionHistory.Core.Services
{
    public interface ITransactionHistoryService
    {
        Task AddAsync(AddTransactionHistoryRequestDto request);
    }
}