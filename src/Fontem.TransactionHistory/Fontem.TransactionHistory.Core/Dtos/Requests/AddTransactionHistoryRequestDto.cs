using System;

namespace Fontem.TransactionHistory.Core.Dtos.Requests
{
    public class AddTransactionHistoryRequestDto
    {
        public Guid OrderId { get; set; }
        public Guid UserId { get; set; }
        public string CorrelationId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}