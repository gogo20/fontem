using System.Threading.Tasks;
using Fontem.Checkout.Core.Models.Events;
using Fontem.TransactionHistory.Core.Dtos.Requests;
using Fontem.TransactionHistory.Core.Services;
using MassTransit;

namespace Fontem.TransactionHistory.Core.Consumers
{
    public class OrderCompletedEventConsumer : IConsumer<OrderCompletedEvent>
    {
        private readonly ITransactionHistoryService _transactionHistoryService;

        public OrderCompletedEventConsumer(ITransactionHistoryService transactionHistoryService)
        {
            _transactionHistoryService = transactionHistoryService;
        }

        public async Task Consume(ConsumeContext<OrderCompletedEvent> context)
        {
            var addTransactionLogRequest = new AddTransactionHistoryRequestDto
            {
                OrderId = context.Message.OrderId,
                UserId = context.Message.UserId,
                CorrelationId = context.Message.CorrelationId,
                CreatedAt = context.Message.CreatedAt
            };

            await _transactionHistoryService.AddAsync(addTransactionLogRequest);
        }
    }
}