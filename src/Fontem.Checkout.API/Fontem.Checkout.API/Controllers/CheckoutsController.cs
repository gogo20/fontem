﻿using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Fontem.Checkout.Core.Dtos.Requests;
using Fontem.Checkout.Core.Dtos.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Fontem.Checkout.API.Controllers
{
    [Route("api/checkouts")]
    [ApiController]
    [Authorize]
    public class CheckoutsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CheckoutsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("{checkoutId}/place-an-order")]
        [SwaggerResponse((int)HttpStatusCode.Created)]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized)]
        public async Task<ActionResult> Checkout([FromRoute] Guid checkoutId, [FromBody] PlaceAnOrderRequestDto request)
        {
            request.OrderId = checkoutId;
            request.RequestedByUserId = Guid.Parse(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value);

            BaseResponseDto placeAnOrderResponse = await _mediator.Send(request);

            if (!placeAnOrderResponse.HasError)
            {
                return Ok();
            }
            else
            {
                var message = placeAnOrderResponse.Messages.FirstOrDefault();

                return StatusCode((int)message?.HttpStatusCode, message?.Message);
            }
        }
    }
}