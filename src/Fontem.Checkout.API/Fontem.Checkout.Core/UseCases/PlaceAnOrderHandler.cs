using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;
using Fontem.Checkout.Core.Dtos.Requests;
using Fontem.Checkout.Core.Dtos.Responses;
using Fontem.Checkout.Core.Models.Events;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Fontem.Checkout.Core.UseCases
{
    public class PlaceAnOrderHandler : IRequestHandler<PlaceAnOrderRequestDto, BaseResponseDto>
    {
        private readonly ILogger<PlaceAnOrderHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IBusControl _busControl;

        public PlaceAnOrderHandler(ILogger<PlaceAnOrderHandler> logger, ICorrelationContextAccessor correlationContextAccessor, IHttpClientFactory httpClientFactory, IBusControl busControl)
        {
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
            _httpClientFactory = httpClientFactory;
            _busControl = busControl;
        }

        public async Task<BaseResponseDto> Handle(PlaceAnOrderRequestDto request, CancellationToken cancellationToken)
        {
           var checkoutResponse = new BaseResponseDto();

           try
           {
               //Some fraud validations
               //Then we need to do payment operation how we want.
               //Then we need to change order status as Payment Completed via reliable API calls to Order API.
               //Then we need to change basket status as Completed via reliable API calls to Basket API.

               //Then we can publish an event, in order to do some operations asynchronously.

               await _busControl.Publish(new OrderCompletedEvent
               {
                   OrderId = request.OrderId,
                   UserId = request.RequestedByUserId,
                   CreatedAt = DateTime.Now,
                   CorrelationId = _correlationContextAccessor.CorrelationContext.CorrelationId
               });
           }
           catch
           {
               //Some operations...
           }

           return checkoutResponse;
        }
    }
}