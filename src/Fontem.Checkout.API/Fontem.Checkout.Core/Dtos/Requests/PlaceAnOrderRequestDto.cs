using System;
using Fontem.Checkout.Core.Dtos.Responses;
using MediatR;
using Newtonsoft.Json;

namespace Fontem.Checkout.Core.Dtos.Requests
{
    public class PlaceAnOrderRequestDto : IRequest<BaseResponseDto>
    {
        [JsonIgnore]
        public Guid OrderId { get; set; }

        [JsonIgnore]
        public Guid RequestedByUserId { get; set; }
        
        public string CardNumber { get; set; }
    }
}