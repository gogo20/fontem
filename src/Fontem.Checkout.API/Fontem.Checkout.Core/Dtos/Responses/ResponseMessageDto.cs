using System.Net;

namespace Fontem.Checkout.Core.Dtos.Responses
{
    public class ResponseMessageDto
    {
        public bool IsError { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public string Message { get; set; }
    }
}