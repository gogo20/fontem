﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Fontem.Auth.Core.Dtos.Requests;
using Fontem.Auth.Core.Dtos.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Fontem.Auth.API.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AuthController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("connect/token")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Request not accepted.")]
        public async Task<IActionResult> GetToken([FromBody] AuthenticationRequestDto request)
        {
            BaseResponseDto<AuthenticationResponseDto> authResponse = await _mediator.Send(request);

            if (!authResponse.HasError && authResponse.Data != null)
            {
                return Ok(authResponse.Data);
            }
            else
            {
                var message = authResponse.Messages.FirstOrDefault();

                return StatusCode((int)message.HttpStatusCode, message.Message);
            }
        }
    }
}
