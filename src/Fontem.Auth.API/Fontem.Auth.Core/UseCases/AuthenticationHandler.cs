using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;
using Fontem.Auth.Core.Dtos.Requests;
using Fontem.Auth.Core.Dtos.Responses;
using Fontem.Auth.Core.Models;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Fontem.Auth.Core.UseCases
{
    public class AuthenticationHandler : IRequestHandler<AuthenticationRequestDto, BaseResponseDto<AuthenticationResponseDto>>
    {
        private readonly ILogger<AuthenticationHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IConfiguration _configuration;

        public AuthenticationHandler(ILogger<AuthenticationHandler> logger, ICorrelationContextAccessor correlationContextAccessor, IConfiguration configuration)
        {
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
            _configuration = configuration;
        }

        public Task<BaseResponseDto<AuthenticationResponseDto>> Handle(AuthenticationRequestDto request, CancellationToken cancellationToken)
        {
            var authenticationResponse = new BaseResponseDto<AuthenticationResponseDto>();

            try
            {
                BaseResponseDto parametersValidationResult = ValidateParameters(request);

                if (parametersValidationResult.HasError)
                {
                    authenticationResponse.Messages.AddRange(parametersValidationResult.Messages);

                    return Task.FromResult(authenticationResponse);
                }

                UserEntity user = null;
                
                //Some user validation & fetching operations...
                if(request.Email == "demo@demo.com" && request.Password == "demo")
                {
                    user = new UserEntity
                    {
                        Id = Guid.Parse("e48a8886-bff8-4915-99cb-65d9b38553ef"), //in order to test
                        Name = "Gökhan",
                        Email = "gok.gokalp@yahoo.com",
                        Password = "123456"
                    };
                }

                if(user != null)
                {
                    (string, int)generateToken = GenerateAccessTokenWithExpireTime(request, user);

                    authenticationResponse.Data = new AuthenticationResponseDto
                    {
                        AccessToken = generateToken.Item1,
                        ExpiresIn = generateToken.Item2
                    };
                }
                else
                {
                    string message = $"Credentials are invalid. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                    authenticationResponse.Messages.Add(new ResponseMessageDto
                    {
                        IsError = true,
                        HttpStatusCode = HttpStatusCode.Unauthorized
                    });
                }
            }
            catch (Exception ex)
            {
                string message = $"An error occurred while authentication operation. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                _logger.LogError(ex, message);

                authenticationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = message
                });
            }

            return Task.FromResult(authenticationResponse);
        }

        private BaseResponseDto ValidateParameters(AuthenticationRequestDto request)
        {
            //If we have specific validations...
            
            var validationResponse = new BaseResponseDto();

            if (string.IsNullOrEmpty(request.Email))
            {
                validationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Message = $"{request.Email.GetType().Name} parameter can not be null."
                });
            }

            return validationResponse;
        }

        private(string, int)GenerateAccessTokenWithExpireTime(AuthenticationRequestDto request, UserEntity user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("TokenSecret"));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Sid, $"{user.Id.ToString()}")
                }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };

            int expiresIn = expiresIn = _configuration.GetValue<int>("TokenExpireTimeInMinute");

            tokenDescriptor.Expires = DateTime.UtcNow.AddMinutes(expiresIn);

            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return (tokenHandler.WriteToken(token), expiresIn);
        }
    }
}
