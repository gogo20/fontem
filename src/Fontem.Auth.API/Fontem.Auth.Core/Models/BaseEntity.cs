using System;
using System.ComponentModel.DataAnnotations;

namespace Fontem.Auth.Core.Models
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}