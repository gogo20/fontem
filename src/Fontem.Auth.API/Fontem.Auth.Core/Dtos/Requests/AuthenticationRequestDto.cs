using System.ComponentModel.DataAnnotations;
using Fontem.Auth.Core.Dtos.Responses;
using MediatR;

namespace Fontem.Auth.Core.Dtos.Requests
{
    public class AuthenticationRequestDto : IRequest<BaseResponseDto<AuthenticationResponseDto>>
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}