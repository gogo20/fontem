using Newtonsoft.Json;

namespace Fontem.Auth.Core.Dtos.Responses
{
    public class AuthenticationResponseDto
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }
}