﻿using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Fontem.Basket.Core.Dtos;
using Fontem.Basket.Core.Dtos.Requests;
using Fontem.Basket.Core.Dtos.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Fontem.Basket.API.Controllers
{
    [Route("api/baskets")]
    [ApiController]
    [Authorize]
    public class BasketsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BasketsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{basketId}")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<GetCustomersActiveBasketResponseDto>> GetCustomersActiveBasket([FromRoute] Guid basketId)
        {
            BaseResponseDto<GetCustomersActiveBasketResponseDto> getCustomersActiveBasket = await _mediator.Send(new GetCustomersActiveBasketRequestDto
            {
                BasketId = basketId,
                RequestedByUserId = Guid.Parse(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value)
            });

            if (!getCustomersActiveBasket.HasError)
            {
                return Ok(getCustomersActiveBasket.Data);
            }
            else
            {
                ResponseMessageDto message = getCustomersActiveBasket.Messages.FirstOrDefault();

                return StatusCode((int)message.HttpStatusCode, message.Message);
            }
        }

        [HttpPost("{basketId}/items")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<GetCustomersActiveBasketResponseDto>> AddItemToCustomersActiveBasket([FromRoute] Guid basketId, [FromBody]AddItemToCustomersActiveBasketRequestDto request)
        {
            request.BasketId = basketId;
            request.RequestedByUserId = Guid.Parse(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value);

            BaseResponseDto<bool> addItemToCustomersActiveBasketResponse = await _mediator.Send(request);

            if (!addItemToCustomersActiveBasketResponse.HasError && addItemToCustomersActiveBasketResponse.Data)
            {
                return Ok();
            }
            else
            {
                ResponseMessageDto message = addItemToCustomersActiveBasketResponse.Messages.FirstOrDefault();

                return StatusCode((int)message?.HttpStatusCode, message?.Message);
            }
        }

        [HttpPost("{basketId}/checkout")]
        [SwaggerResponse((int)HttpStatusCode.Created)]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized)]
        public async Task<ActionResult> Checkout([FromRoute] Guid basketId)
        {
            BaseResponseDto checkoutResponse = await _mediator.Send(new CheckoutRequestDto
            {
                BasketId = basketId,
                RequestedByUserId = Guid.Parse(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value)
            });

            if (!checkoutResponse.HasError)
            {
                return Created($"checkouts/{basketId.ToString()}", null);
            }
            else
            {
                ResponseMessageDto message = checkoutResponse.Messages.FirstOrDefault();

                return StatusCode((int)message?.HttpStatusCode, message?.Message);
            }
        }
    }
}