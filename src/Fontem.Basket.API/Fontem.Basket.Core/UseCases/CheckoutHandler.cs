using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;
using Fontem.Basket.Core.Common;
using Fontem.Basket.Core.Dtos.Requests;
using Fontem.Basket.Core.Dtos.Responses;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Fontem.Basket.Core.UseCases
{
    public class CheckoutHandler : IRequestHandler<CheckoutRequestDto, BaseResponseDto>
    {
        private readonly ILogger<CheckoutHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;

        public CheckoutHandler(ILogger<CheckoutHandler> logger, ICorrelationContextAccessor correlationContextAccessor, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
            _httpClientFactory = httpClientFactory;
        }

        public Task<BaseResponseDto> Handle(CheckoutRequestDto request, CancellationToken cancellationToken)
        {
           var checkoutResponse = new BaseResponseDto();

           try
           {
               HttpClient client = _httpClientFactory.CreateClient(Constants.ResilientHttpClient); 

               // Normally we should call the Order API in order to create an order. Let's just assume we are calling API and doing some validations...
           }
           catch (System.Exception ex)
           {
               //Some operations...
           }

           return Task.FromResult(checkoutResponse);
        }
    }
}