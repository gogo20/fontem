using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;
using Fontem.Basket.Core.Dtos.Requests;
using Fontem.Basket.Core.Dtos.Responses;
using Fontem.Basket.Core.Helpers;
using Fontem.Basket.Core.Interfaces;
using Fontem.Basket.Core.Models;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Fontem.Basket.Core.UseCases
{
    public class AddItemToCustomersActiveBasketHandler : IRequestHandler<AddItemToCustomersActiveBasketRequestDto, BaseResponseDto<bool>>
    {
        private readonly IRepository<BasketEntity> _basketRepository;
        private readonly ILogger<AddItemToCustomersActiveBasketHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IBasketHelper _basketHelper;

        public AddItemToCustomersActiveBasketHandler(IRepository<BasketEntity> basketRepository, ILogger<AddItemToCustomersActiveBasketHandler> logger, ICorrelationContextAccessor correlationContextAccessor, IBasketHelper basketHelper)
        {
            _basketRepository = basketRepository;
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
            _basketHelper = basketHelper;
        }

        public async Task<BaseResponseDto<bool>> Handle(AddItemToCustomersActiveBasketRequestDto request, CancellationToken cancellationToken)
        {
            var addItemToCustomersActiveBasketResponse = new BaseResponseDto<bool>();

            try
            {
                BaseResponseDto parametersValidationResult = ValidateParameters(request);

                if (parametersValidationResult.HasError)
                {
                    addItemToCustomersActiveBasketResponse.Messages.AddRange(parametersValidationResult.Messages);

                    return addItemToCustomersActiveBasketResponse;
                }

                BasketEntity basket = (await _basketRepository.GetWhereAsync(x => x.Id == request.BasketId && x.UserId == request.RequestedByUserId && x.IsCompleted == false));
                
                DateTime currentDate = DateTime.UtcNow;
                if (basket == null)
                {
                    basket = new BasketEntity
                    {
                        Id = request.BasketId,
                        UserId = request.RequestedByUserId,
                        CreatedAt = currentDate,
                        Items = new List<BasketItemEntity>()
                    };
                }

                basket.ModifiedAt = currentDate;

                basket.Items.Add(new BasketItemEntity
                {
                    Id = Guid.NewGuid(),
                    CryptocurrencyId = request.CryptocurrencyId,
                    CryptocurrencyName = request.CryptocurrencyName,
                    Amount = request.Amount,
                    Price = request.Price,
                    PriceCurrencyCode = request.PriceCurrencyCode,
                    CreatedAt = currentDate,
                    ModifiedAt = currentDate
                });

                decimal totalPriceOfTheBasket = _basketHelper.CalculateTotalPriceOfTheBasket(basket);

                basket.TotalPrice = totalPriceOfTheBasket;
                basket.TotalPriceCurrencyCode = request.PriceCurrencyCode;

                if (basket.CreatedAt == currentDate)
                {
                    await _basketRepository.CreateAsync(basket);
                }
                else
                {
                    await _basketRepository.UpdateAsync(basket);
                }

                addItemToCustomersActiveBasketResponse.Data = true;
            }
            catch (Exception ex)
            {
                string message = $"An error occurred while adding item to the basket. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                _logger.LogError(ex, message);

                addItemToCustomersActiveBasketResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = message
                });
            }

            return addItemToCustomersActiveBasketResponse;
        }

        private BaseResponseDto ValidateParameters(AddItemToCustomersActiveBasketRequestDto request)
        {
            //If we have specific validations...

            var validationResponse = new BaseResponseDto();

            if (string.IsNullOrEmpty(request.CryptocurrencyName))
            {
                validationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Message = $"{request.CryptocurrencyName.GetType().Name} parameter can not be null. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                });
            }

            return validationResponse;
        }
    }
}