using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;
using Fontem.Basket.Core.Dtos;
using Fontem.Basket.Core.Dtos.Requests;
using Fontem.Basket.Core.Dtos.Responses;
using Fontem.Basket.Core.Interfaces;
using Fontem.Basket.Core.Models;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Fontem.Basket.Core.UseCases
{
    public class GetCustomersActiveBasketHandler : IRequestHandler<GetCustomersActiveBasketRequestDto, BaseResponseDto<GetCustomersActiveBasketResponseDto>>
    {
        private readonly IRepository<BasketEntity> _basketRepository;
        private readonly ILogger<GetCustomersActiveBasketHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;

        public GetCustomersActiveBasketHandler(IRepository<BasketEntity> basketRepository, ILogger<GetCustomersActiveBasketHandler> logger, ICorrelationContextAccessor correlationContextAccessor)
        {
            _basketRepository = basketRepository;
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
        }

        public async Task<BaseResponseDto<GetCustomersActiveBasketResponseDto>> Handle(GetCustomersActiveBasketRequestDto request, CancellationToken cancellationToken)
        {
            var getCustomersActiveBasketResponse = new BaseResponseDto<GetCustomersActiveBasketResponseDto>();

            try
            {
                BasketEntity basket = (await _basketRepository.GetWhereAsync(x => x.Id == request.BasketId && x.UserId == request.RequestedByUserId && x.IsCompleted == false));

                getCustomersActiveBasketResponse.Data = new GetCustomersActiveBasketResponseDto();

                if (basket != null)
                {
                    getCustomersActiveBasketResponse.Data.Id = basket.Id;
                    getCustomersActiveBasketResponse.Data.TotalPrice = basket.TotalPrice;
                    getCustomersActiveBasketResponse.Data.TotalPriceCurrencyCode = basket.TotalPriceCurrencyCode;
                    getCustomersActiveBasketResponse.Data.Items = basket.Items.Select(x => new GetCustomersActiveBasketItemResponseDto
                    {
                        Id = x.Id,
                        CryptocurrencyName = x.CryptocurrencyName,
                        Amount = x.Amount,
                        Price = x.Price,
                        PriceCurrencyCode = x.PriceCurrencyCode
                    }).ToList();
                }
                else
                {
                    getCustomersActiveBasketResponse.Data.Id = request.BasketId;
                    getCustomersActiveBasketResponse.Data.Items = new List<GetCustomersActiveBasketItemResponseDto>();
                }
            }
            catch (Exception ex)
            {
                string message = $"An error occurred while getting basket. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                _logger.LogError(ex, message);

                getCustomersActiveBasketResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = message
                });
            }

            return getCustomersActiveBasketResponse;
        }
    }
}