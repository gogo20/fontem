using Fontem.Basket.Core.Models;

namespace Fontem.Basket.Core.Helpers
{
    public interface IBasketHelper
    {
        decimal CalculateTotalPriceOfTheBasket(BasketEntity activeBasket);
    }
}