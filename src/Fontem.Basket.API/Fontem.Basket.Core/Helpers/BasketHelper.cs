using Fontem.Basket.Core.Models;

namespace Fontem.Basket.Core.Helpers
{
    public class BasketHelper : IBasketHelper
    {
        public decimal CalculateTotalPriceOfTheBasket(BasketEntity activeBasket)
        {
            decimal totalPrice = 0;

            if (activeBasket == null || activeBasket.Items == null)
            {
                return totalPrice;
            }

            foreach (var item in activeBasket.Items)
            {
                totalPrice += item.Price;
            }

            return totalPrice;
        }
    }
}