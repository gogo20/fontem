using System;
using System.ComponentModel.DataAnnotations;

namespace Fontem.Basket.Core.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
}