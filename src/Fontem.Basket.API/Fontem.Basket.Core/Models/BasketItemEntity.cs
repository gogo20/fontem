namespace Fontem.Basket.Core.Models
{
    public class BasketItemEntity : BaseEntity
    {
        public int CryptocurrencyId { get; set; }
        public string CryptocurrencyName { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
        public string PriceCurrencyCode { get; set; }
    }
}