using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fontem.Basket.Core.Models
{
    [Table("Basket")]
    public class BasketEntity : BaseEntity
    {
        public Guid UserId { get; set; }
        public decimal TotalPrice { get; set; }
        public string TotalPriceCurrencyCode { get; set; }
        public bool IsCompleted { get; set; }
        public ICollection<BasketItemEntity> Items { get; set; }
    }
}