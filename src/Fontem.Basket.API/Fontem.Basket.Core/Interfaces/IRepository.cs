using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Fontem.Basket.Core.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task CreateAsync(T entity);
        Task UpdateAsync(T entity);
        Task<T> GetWhereAsync(Expression<Func<T, bool>> predicate);
        Task<T> GetAsync(Guid id);
    }
}