using System;
using System.Collections.Generic;
using Fontem.Basket.Core.Dtos.Responses;

namespace Fontem.Basket.Core.Dtos
{
    public class GetCustomersActiveBasketResponseDto
    {
        public Guid Id { get; set; }
        public decimal TotalPrice { get; set; }
        public string TotalPriceCurrencyCode { get; set; }
        public List<GetCustomersActiveBasketItemResponseDto> Items { get; set; }
    }
}