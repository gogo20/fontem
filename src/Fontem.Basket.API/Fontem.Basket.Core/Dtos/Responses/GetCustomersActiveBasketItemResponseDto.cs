using System;

namespace Fontem.Basket.Core.Dtos.Responses
{
    public class GetCustomersActiveBasketItemResponseDto
    {
        public Guid Id { get; set; }
        public string CryptocurrencyName { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
        public string PriceCurrencyCode { get; set; }
    }
}