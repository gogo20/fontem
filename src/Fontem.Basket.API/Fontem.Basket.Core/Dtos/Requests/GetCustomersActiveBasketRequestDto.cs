using System;
using Fontem.Basket.Core.Dtos.Responses;
using MediatR;
using Newtonsoft.Json;

namespace Fontem.Basket.Core.Dtos.Requests
{
    public class GetCustomersActiveBasketRequestDto : IRequest<BaseResponseDto<GetCustomersActiveBasketResponseDto>>
    {
        public Guid BasketId { get; set; }

        public Guid RequestedByUserId { get; set; }
    }
}