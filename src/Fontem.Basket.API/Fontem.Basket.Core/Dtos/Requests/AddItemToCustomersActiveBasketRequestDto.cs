using System;
using System.ComponentModel.DataAnnotations;
using Fontem.Basket.Core.Dtos.Responses;
using MediatR;
using Newtonsoft.Json;

namespace Fontem.Basket.Core.Dtos.Requests
{
    public class AddItemToCustomersActiveBasketRequestDto : IRequest<BaseResponseDto<bool>>
    {
        [JsonIgnore]
        public Guid RequestedByUserId { get; set; }

        [JsonIgnore]
        public Guid BasketId { get; set; }

        [Required]
        public int CryptocurrencyId { get; set; }

        [Required]
        public string CryptocurrencyName { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public string PriceCurrencyCode { get; set; }
    }
}
