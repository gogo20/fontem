using System;
using Fontem.Basket.Core.Dtos.Responses;
using MediatR;

namespace Fontem.Basket.Core.Dtos.Requests
{
    public class CheckoutRequestDto : IRequest<BaseResponseDto>
    {
        public Guid RequestedByUserId { get; set; }
        public Guid BasketId { get; set; }
    }
}