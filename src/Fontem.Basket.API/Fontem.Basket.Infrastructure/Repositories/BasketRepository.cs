using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Fontem.Basket.Core.Interfaces;
using Fontem.Basket.Core.Models;
using MongoDB.Driver;

namespace Fontem.Basket.Infrastructure.Repositories
{
    public class BasketRepository : IRepository<BasketEntity>
    {
        private readonly AppContext _appContext;

        public BasketRepository(AppContext appContext)
        {
            _appContext = appContext;
        }

        public async Task CreateAsync(BasketEntity entity)
        {
            await _appContext.Baskets.InsertOneAsync(entity);
        }

        public async Task<BasketEntity> GetAsync(Guid id)
        {
            BasketEntity basket = await _appContext.Baskets.Find(x => x.Id == id).FirstOrDefaultAsync();

            return basket;
        }

        public async Task<BasketEntity> GetWhereAsync(Expression<Func<BasketEntity, bool>> predicate)
        {
            BasketEntity basket = await _appContext.Baskets.Find(predicate).FirstOrDefaultAsync();

            return basket;
        }

        public async Task UpdateAsync(BasketEntity entity)
        {
            await _appContext.Baskets.ReplaceOneAsync(x => x.Id == entity.Id, entity, options : new UpdateOptions { IsUpsert = true });
        }
    }
}