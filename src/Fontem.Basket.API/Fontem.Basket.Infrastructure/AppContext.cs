﻿using Fontem.Basket.Core.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Fontem.Basket.Infrastructure
{
    public class AppContext
    {
        private readonly IMongoDatabase _mongoDatabase;

        public AppContext(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetValue<string>("MongoDB_Connection_String"));

            if (client != null)
            {
                _mongoDatabase = client.GetDatabase(configuration.GetValue<string>("MongoDB_DBName"));
            }
        }

        public IMongoCollection<BasketEntity> Baskets => _mongoDatabase.GetCollection<BasketEntity>("Basket");
    }
}