using System.Linq;
using AutoFixture;
using FluentAssertions;
using Fontem.Basket.Core.Helpers;
using Fontem.Basket.Core.Models;
using NUnit.Framework;

namespace Fontem.Basket.Tests.UnitTests
{
    [TestFixture]
    public class BasketHelperTests
    {
        private IFixture _fixture;
        private IBasketHelper _basketHelper;

        [SetUp]
        public void SetUp()
        {
            _fixture = new Fixture();
            _basketHelper = new BasketHelper();
        }

        [Test]
        public void CalculateTotalPriceOfTheBasket_WhenABasketEntityPassedThatContainsOneItem_ShouldCalculateTotalPriceAndReturnIt()
        {
            //Arrange
            var basketEntity = _fixture.Create<BasketEntity>();
            var basketItemEntity = _fixture.Create<BasketItemEntity>();

            basketEntity.Items.Clear();
            basketEntity.Items.Add(basketItemEntity);

            //Act
            decimal totalPrice = _basketHelper.CalculateTotalPriceOfTheBasket(basketEntity);

            //Assert
            totalPrice.Should().Be(basketItemEntity.Price);
        }

        [Test]
        public void CalculateTotalPriceOfTheBasket_WhenABasketEntityPassedThatContainsMultipleItems_ShouldCalculateTotalPriceAndReturnIt()
        {
            //Arrange
            var basketEntity = _fixture.Create<BasketEntity>();
            var basketItemEntity = _fixture.Create<BasketItemEntity>();
            basketEntity.Items.Add(basketItemEntity);

            decimal actualTotalPrice = 0;

            basketEntity.Items.ToList().ForEach(x => actualTotalPrice += x.Price);

            //Act
            decimal expectedTotalPrice = _basketHelper.CalculateTotalPriceOfTheBasket(basketEntity);

            //Assert
            expectedTotalPrice.Should().Be(actualTotalPrice);
        }

        [Test]
        public void CalculateTotalPriceOfTheBasket_WhenABasketEntityPassedAsNull_ShouldCalculateTotalPriceAs0AndReturnIt()
        {
            //Arrange
            BasketEntity basketEntity = null;
            decimal actualTotalPrice = 0;

            //Act
            decimal expectedTotalPrice = _basketHelper.CalculateTotalPriceOfTheBasket(basketEntity);

            //Assert
            expectedTotalPrice.Should().Be(actualTotalPrice);
        }

        [Test]
        public void CalculateTotalPriceOfTheBasket_WhenABasketEntityPassedWithNullItems_ShouldCalculateTotalPriceAs0AndReturnIt()
        {
            //Arrange
            var basketEntity = _fixture.Create<BasketEntity>();
            basketEntity.Items = null;
            
            decimal actualTotalPrice = 0;

            //Act
            decimal expectedTotalPrice = _basketHelper.CalculateTotalPriceOfTheBasket(basketEntity);

            //Assert
            expectedTotalPrice.Should().Be(actualTotalPrice);
        }
    }
}