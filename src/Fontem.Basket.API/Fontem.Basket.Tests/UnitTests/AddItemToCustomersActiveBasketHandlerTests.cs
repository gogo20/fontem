using System;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using CorrelationId;
using FluentAssertions;
using Fontem.Basket.Core.Dtos.Requests;
using Fontem.Basket.Core.Dtos.Responses;
using Fontem.Basket.Core.Helpers;
using Fontem.Basket.Core.Interfaces;
using Fontem.Basket.Core.Models;
using Fontem.Basket.Core.UseCases;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Fontem.Basket.Tests.UnitTests
{
    [TestFixture]
    public class AddItemToCustomersActiveBasketHandlerTests
    {
        private IFixture _fixture;
        private Mock<ILogger<AddItemToCustomersActiveBasketHandler>> _mockLogger;
        private Mock<ICorrelationContextAccessor> _mockCorrelationContextAccessor;
        private Mock<IRepository<BasketEntity>> _mockBasketRepository;
        private string _correlationId;
        private Mock<IBasketHelper> _basketHelper;
        private AddItemToCustomersActiveBasketHandler _addItemToCustomersActiveBasketHandler;

        [SetUp]
        public void SetUp()
        {
            _fixture = new Fixture();
            _mockLogger = new Mock<ILogger<AddItemToCustomersActiveBasketHandler>>();
            _correlationId = _fixture.Create<string>();
            _mockCorrelationContextAccessor = new Mock<ICorrelationContextAccessor>();

            var correlationContextFactory = new CorrelationContextFactory();

            _mockCorrelationContextAccessor.SetupGet(_ => _.CorrelationContext)
                .Returns(correlationContextFactory.Create(_correlationId, _fixture.Create<string>()));

            _basketHelper = new Mock<IBasketHelper>();
            _mockBasketRepository = new Mock<IRepository<BasketEntity>>();

            _addItemToCustomersActiveBasketHandler = new AddItemToCustomersActiveBasketHandler(_mockBasketRepository.Object, _mockLogger.Object, _mockCorrelationContextAccessor.Object, _basketHelper.Object);
        }

        [Test]
        public async Task Handle_WhenACustomerTriesToAddAnItemToNonExistingBasket_ShouldCreateBasketThenAddItemToTheBasketAndReturnSuccessfullyAResponse()
        {
            // Arrange
            var addItemToCustomersActiveBasketRequestDto = _fixture.Create<AddItemToCustomersActiveBasketRequestDto>();
            BasketEntity basketEntity = null;
            var cancellationToken = _fixture.Create<CancellationToken>();
            decimal total = addItemToCustomersActiveBasketRequestDto.Price;

            _mockBasketRepository.Setup(_ => _.GetWhereAsync(It.IsAny<Expression<Func<BasketEntity, bool>>>())).ReturnsAsync(basketEntity);
            _basketHelper.Setup(_ => _.CalculateTotalPriceOfTheBasket(It.IsAny<BasketEntity>())).Returns(total);

            //Act
            BaseResponseDto<bool> response = await _addItemToCustomersActiveBasketHandler.Handle(addItemToCustomersActiveBasketRequestDto, cancellationToken);

            //Assert
            response.Data.Should().BeTrue();
            response.HasError.Should().BeFalse();
            _basketHelper.Verify(_ => _.CalculateTotalPriceOfTheBasket(It.IsAny<BasketEntity>()));
            _mockBasketRepository.Verify(_ => _.CreateAsync(It.IsAny<BasketEntity>()));
        }

        [Test]
        public async Task Handle_WhenACustomerTriesToAddAnItemToExistingBasket_ShouldAddItemToTheBasketAndReturnSuccessfullyAResponse()
        {
            // Arrange
            var addItemToCustomersActiveBasketRequestDto = _fixture.Create<AddItemToCustomersActiveBasketRequestDto>();
            BasketEntity basketEntity = _fixture.Create<BasketEntity>();
            var cancellationToken = _fixture.Create<CancellationToken>();
            decimal total = addItemToCustomersActiveBasketRequestDto.Price;

            _mockBasketRepository.Setup(_ => _.GetWhereAsync(It.IsAny<Expression<Func<BasketEntity, bool>>>())).ReturnsAsync(basketEntity);
            _basketHelper.Setup(_ => _.CalculateTotalPriceOfTheBasket(It.IsAny<BasketEntity>())).Returns(total);

            //Act
            BaseResponseDto<bool> response = await _addItemToCustomersActiveBasketHandler.Handle(addItemToCustomersActiveBasketRequestDto, cancellationToken);

            //Assert
            response.Data.Should().BeTrue();
            response.HasError.Should().BeFalse();
            _basketHelper.Verify(_ => _.CalculateTotalPriceOfTheBasket(It.IsAny<BasketEntity>()));
            _mockBasketRepository.Verify(_ => _.UpdateAsync(It.IsAny<BasketEntity>()));
        }

        [Test]
        public async Task Handle_WhenACustomerTriesToAddAnItemToExistingBasketWithAnEmtpyCryptocurrencyName_ShouldReturnBadRequestResponseWithDetails()
        {
            // Arrange
            var addItemToCustomersActiveBasketRequestDto = _fixture.Create<AddItemToCustomersActiveBasketRequestDto>();
            var cancellationToken = _fixture.Create<CancellationToken>();
            addItemToCustomersActiveBasketRequestDto.CryptocurrencyName = string.Empty;

            //Act
            BaseResponseDto<bool> response = await _addItemToCustomersActiveBasketHandler.Handle(addItemToCustomersActiveBasketRequestDto, cancellationToken);

            //Assert
            response.Data.Should().BeFalse();
            response.HasError.Should().BeTrue();
            response.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.BadRequest && x.IsError == true && x.Message == $"{addItemToCustomersActiveBasketRequestDto.CryptocurrencyName.GetType().Name} parameter can not be null. Error Code: {_correlationId}");
        }

        [Test]
        public async Task Handle_WhenAnUnexceptedExceptionOccures_ShouldReturnInternalServerErrorResponseWithDetails()
        {
            // Arrange
            var addItemToCustomersActiveBasketRequestDto = _fixture.Create<AddItemToCustomersActiveBasketRequestDto>();
            var cancellationToken = _fixture.Create<CancellationToken>();

            _mockBasketRepository.Setup(_ => _.GetWhereAsync(It.IsAny<Expression<Func<BasketEntity, bool>>>())).Throws(_fixture.Create<Exception>());

            //Act
            BaseResponseDto<bool> response = await _addItemToCustomersActiveBasketHandler.Handle(addItemToCustomersActiveBasketRequestDto, cancellationToken);

            //Assert
            response.Data.Should().BeFalse();
            response.HasError.Should().BeTrue();
            response.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.InternalServerError && x.IsError == true && x.Message == $"An error occurred while adding item to the basket. Error Code: {_correlationId}");
        }
    }
}