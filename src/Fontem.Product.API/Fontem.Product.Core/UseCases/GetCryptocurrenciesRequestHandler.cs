using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Fontem.Product.Core.Dtos;
using Fontem.Product.Core.Dtos.Requests;
using Fontem.Product.Core.Dtos.Responses;
using Fontem.Product.Core.Services;
using MediatR;

namespace Fontem.Product.Core.UseCases
{
    public class GetCryptocurrenciesRequestHandler : IRequestHandler<GetCryptocurrenciesRequestDto, BaseResponseDto<List<ProductDto>>>
    {
        private readonly ICoinService _coinService;

        public GetCryptocurrenciesRequestHandler(ICoinService coinService)
        {
            _coinService = coinService;
        }

        public async Task<BaseResponseDto<List<ProductDto>>> Handle(GetCryptocurrenciesRequestDto request, CancellationToken cancellationToken)
        {
            var productsResponse = new BaseResponseDto<List<ProductDto>>();

            var getProductsResponse = await _coinService.GetCryptocurrenciesAsync(request);

            if(!getProductsResponse.HasError)
            {
                productsResponse.Data = getProductsResponse.Data;
            }
            else
            {
                productsResponse.Messages.AddRange(getProductsResponse.Messages);
            }

            return productsResponse;
        }
    }
}