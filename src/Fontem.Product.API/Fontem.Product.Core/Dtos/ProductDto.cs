namespace Fontem.Product.Core.Dtos
{
    public class ProductDto
    {
        public int CryptocurrencyId { get; set; }
        public string CryptocurrencyName { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }
        public string PriceCurrencyCode { get; set; }
    }
}