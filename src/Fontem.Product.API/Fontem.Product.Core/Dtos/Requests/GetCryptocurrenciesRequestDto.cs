using System.Collections.Generic;
using Fontem.Product.Core.Dtos.Responses;
using MediatR;

namespace Fontem.Product.Core.Dtos.Requests
{
    public class GetCryptocurrenciesRequestDto : IRequest<BaseResponseDto<List<ProductDto>>>
    {
        public string Convert { get; set; }
    }
}