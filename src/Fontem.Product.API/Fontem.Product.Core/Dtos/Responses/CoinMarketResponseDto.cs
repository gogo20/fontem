using System.Collections.Generic;

namespace Fontem.Product.Core.Dtos.Responses
{
    public class CoinMarketResponseDto
    {
        public List<CoinMarketItemResponseDto> Data { get; set; }
    }

    public class CoinMarketItemResponseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CoinMarketMarketQuoteResponseDto Quote { get; set; }
    }

    public class CoinMarketMarketQuoteResponseDto
    {
        public CoinMarketMarketUSDQuoteResponseDto USD { get; set; }
    }

    public class CoinMarketMarketUSDQuoteResponseDto
    {
        public decimal Price { get; set; }
    }
}