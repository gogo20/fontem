using System.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CorrelationId;
using Fontem.Product.Core.Common;
using Fontem.Product.Core.Dtos;
using Fontem.Product.Core.Dtos.Requests;
using Fontem.Product.Core.Dtos.Responses;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Fontem.Product.Core.Services
{
    public class CoinMarketService : ICoinService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<CoinMarketService> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private string _coinMarketBaseURI;
        private string _coinMarketAPIKey;

        public CoinMarketService(IConfiguration configuration, ILogger<CoinMarketService> logger, ICorrelationContextAccessor correlationContextAccessor,
            IHttpClientFactory httpClientFactory)
        {
            _configuration = configuration;
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
            _httpClientFactory = httpClientFactory;
            _coinMarketBaseURI = _configuration.GetValue<string>("Coin_Market_Base_URI");
            _coinMarketAPIKey = _configuration.GetValue<string>("Coin_Market_API_Key");
        }

        public async Task<BaseResponseDto<List<ProductDto>>> GetCryptocurrenciesAsync(GetCryptocurrenciesRequestDto request)
        {
            var productsResponse = new BaseResponseDto<List<ProductDto>>();

            try
            {
                HttpClient client = _httpClientFactory.CreateClient(Constants.ResilientHttpClient);

                client.BaseAddress = new Uri(_coinMarketBaseURI);
                client.DefaultRequestHeaders.Add("X-CMC_PRO_API_KEY", _coinMarketAPIKey);

                string cryptocurrencyListingResourceURI = _configuration.GetValue<string>("Coin_Market_Cryptocurrency_Listing_Resource_URI");
                string response = await client.GetStringAsync(cryptocurrencyListingResourceURI);

                productsResponse.Data = new List<ProductDto>();

                if(string.IsNullOrEmpty(response))
                {
                    return productsResponse;
                }     

                CoinMarketResponseDto coinMarketResponse = JsonConvert.DeserializeObject<CoinMarketResponseDto>(response);

                if(coinMarketResponse.Data != null && coinMarketResponse.Data.Count > 0)
                {
                    productsResponse.Data.AddRange(coinMarketResponse.Data.Select(x => new ProductDto
                    {
                        CryptocurrencyId = x.Id,
                        CryptocurrencyName = x.Name,
                        Amount = 1,
                        Price = (decimal)x.Quote?.USD?.Price,
                        PriceCurrencyCode = "USD"
                    }));
                }
            }
            catch (Exception ex)
            {
                string message = $"An error occurred while getting products. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                _logger.LogError(ex, message);

                productsResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = message
                });
            }

            return productsResponse;
        }
    }
}