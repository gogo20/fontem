using System.Collections.Generic;
using System.Threading.Tasks;
using Fontem.Product.Core.Dtos;
using Fontem.Product.Core.Dtos.Requests;
using Fontem.Product.Core.Dtos.Responses;

namespace Fontem.Product.Core.Services
{
    public interface ICoinService
    {
         Task<BaseResponseDto<List<ProductDto>>> GetCryptocurrenciesAsync(GetCryptocurrenciesRequestDto request);
    }
}