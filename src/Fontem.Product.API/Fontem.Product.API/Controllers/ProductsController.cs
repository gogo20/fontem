﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Fontem.Product.Core.Dtos;
using Fontem.Product.Core.Dtos.Requests;
using Fontem.Product.Core.Dtos.Responses;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Fontem.Product.API.Controllers
{
    [Route("api/products")]
    [ApiController]
    [Authorize]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet()]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.Unauthorized)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<List<ProductDto>>> GetCustomersActiveBasket([FromQuery] GetCryptocurrenciesRequestDto request)
        {
            BaseResponseDto<List<ProductDto>> getProducts = await _mediator.Send(request);

            if (!getProducts.HasError)
            {
                return Ok(getProducts.Data);
            }
            else
            {
                ResponseMessageDto message = getProducts.Messages.FirstOrDefault();

                return StatusCode((int)message.HttpStatusCode, message.Message);
            }
        }
    }
}