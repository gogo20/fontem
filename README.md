# Fontem Case

## Task

The goal of the assignment is to give us insight on how confident you are with the c# stack in a e-commerce context

Description/Requirements
This assignment is to build the backend of a simple web commerce cart by using crypto currencies as product. 
Create an API that allow a user to buy crypto currencies from a list. 
The crypto currency data is pulled from https://coinmarketcap.com/api/ .

The api endpoints are:
 `GET: /products`
 `POST: /purchase`

All purchases are persisted with a price information, that refers to the price of the cryptocurrency at the moment of the purchase and the id of the user who made the purchase.

At least 80% of the code must be covered with unit tests and integration tests
If you feel you can do more, here some __optional__ requirements:

- endpoints are authenticated
- function programming style, by using, for instance https://github.com/louthy/language-ext
- use docker to run the project

Delivery:
- the project must be easily started by using Rider/Visual Studio Code/Visual Studio
- .NET Core version: latest, c# version: latest.
- the project must be pushed in a github/gitlab/bitbucket repository
- HTTP API collection with POSTMAN

### Assumptions

- I assumed each user can have one basket.
- I assumed user id can be used as basket id.
- I assumed transaction logs can be persisted asynchronous.
- I assumed we will just show as USD.

### Solutions & Tech

- For all projects architecture, I used minimal clean architecture(ports&adapters) in order to design domain-centric and fully loosely-coupled architecture.
- I used MediatR package in order to provide fully single responsible and loosely-coupled communication between layers.
- I used the correlation id package in order to correlate logs across services.
- I used Polly in order to do reliable API communications. (we can use some timeouts, retry mechanism etc.)
- I used mongodb for the Fontem.Basket.API.
- I just wrote some unit tests for the Fontem.Basket.API project in order to show how I'm writing tests. Normally I could write fully tests and integration tests but I didn't have enough time, these days I'm a little bit busy. (vacation etc.).
- For authentication operation, I just implemented JWT token generation operations in Fontem.Auth.API project for simplicity. Normally we can use IdentityServer.
- In this way, each APIs have an authentication mechanism. I just used user-credential flow. APIs are resolving tokens via a shared key. BTW If I used IdentityServer, I could use IdentityServer as authority for resolving tokens.
- I created 5 projects as microservices.
- I designd APIs as a RESTful.
- I created Fontem.Checkout.API as an orchestrator. I also added some commets in it.
- I also used RabbitMQ as a message broker. I used MetroBus package for reliable communication as a service bus.
- I created a docker-compose file in order to test easily.
- I also impemented Swagger API.

### Usage

- First we need to get access token via Auth API. It will serve on http://localhost:5005 - POST /api/auth/connect/token
- Credentials: email: "demo@demo.com", password: "demo".
- In order to get products we need to access Product API via http://localhost:5002 - GET /api/products
- In order to add items to the basket, we need to use Basket API via http://localhost:5004 - POST /api/baskets/{basketId}/items - "basketId" is the "customerId" that is in the token: e48a8886-bff8-4915-99cb-65d9b38553ef
- In order to get basket, we need to use Basket API via http://localhost:5004 - GET /api/baskets/{basketId}
- In order to start checkout operation, we need to use Basket API via http://localhost:5004 - POST /api/baskets/{basketId}/checkout
- In order to complete checkout operation, we need to use CheckOut API via http://localhost:5003 - POST /api/checkouts/{checkoutId}/place-an-order - checkout id is the basketId also. Normally it should be order id.
- When checkout operation completed, an event will be raised called "OrderCompletedEvent".
- In order to create some transaction histories, I developed a consumer which will be subscribed to the "OrderCompletedEvent" event.

This is the project that I developed in 3 days.

Regards.
